<!-- Headings -->
# SecurityServ

Ce playbook ansible permet de sécuriser votre serveur en automatisant la mise en place :
* Règles firewall avec iptables
* Prévention des attaques par brute force avec Fail2ban
* Blocage des scans de port avec Portsentry

# Requirements
1. Changer le host dans le fichier **host.yml**
2. Changer si besoin les règles firewall dans le fichier **roles/iptables/templates/firewall.j2**
3. Changer si besoin les règles fail2ban, par exemple le port ssh ou les times ban, dans le fichier **roles/fail2ban/templates/jail.conf.j2**

# Usage

Definir la variable **ssh_port**  dans le fichier **playbook.yml**. Sa valeur correspond à votre port ssh (par defaut 22)

Puis pour lancer le playbook :

```bash
ansible-playbook -i hosts.yml --user <your user> -b --ask-pass playbook.yml
```
